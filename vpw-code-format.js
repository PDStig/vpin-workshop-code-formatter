// ChatGPT was used to assist with the starting development of some of the functions.

window.onload = () => {
  let data1 = `Class aRandomClass
	Public Sub doSomething(mode)
		If mode = 5 Ihen
			Dim i
			For Each i In Lights
				Lights.state = 0
			Next
		ElseIf mode < 5 Then
			Select Case mode
				Case 1
					Lights(1).state = 1
					With NothingBurger
						.Add pickles
						.MakeUseful()
					End With
				Case 2
					Lights(2).state = 2
					PlaySound "trumpet"
				Case Else
					PlaySound "Nothing"
			End Select
		Else
			Lights(4).state = 2
			With SomeClass
				.Add "me", 1
				.Add "you", 0
				.Reset
			End With
		End If
	End Sub
End Class`;
  data1 = fixIndentation(data1);
  document.getElementById("example1").value = data1;

  let data2 = `unrelatedCode()

'This is a comment
relatedCode()
If related = True Then
moreRelatedCode()
related = false
End If

unrelatedCode()

If stuff = False Then
'Another comment
relatedCode()
End If`;
  data2 = fixIndentation(data2);
  document.getElementById("example2").value = data2;
};

/**
 * Called from HTML; format code.
 */
function formatCode() {
  // Get input code
  let inputCode = document.getElementById("inputCode");
  let data = inputCode.value;

  // Format operations

  let doCapitalizeKeywords = document.getElementById(
    "formatCapitalizeKeywords"
  );
  if (doCapitalizeKeywords && doCapitalizeKeywords.checked) {
    data = capitalizeKeywords(data);
  }

  let doRemoveChainedCode = document.getElementById("formatRemoveChainedCode");
  if (doRemoveChainedCode && doRemoveChainedCode.checked) {
    data = removeChainedCode(data);
  }

  let doFixIndentation = document.getElementById("formatFixIndentation");
  if (doFixIndentation && doFixIndentation.checked) {
    data = fixIndentation(data);
  }

  // Output modified code
  let outputCode = document.getElementById("outputCode");
  outputCode.value = data;
}

/**
 * Helper function to replace strings in vbscript code unless it is in a comment.
 *
 * @param {string} vbscriptCode The vbscript code
 * @param {string|RegExp} searchString The string or regex to find
 * @param {string} replaceString The string to which all searchString occurrences are changed
 * @returns The modified vbscript code
 */
function _replaceVBScriptCode(vbscriptCode, searchString, replaceString) {
  // Process line-by-line
  let lines = vbscriptCode.split("\n");
  for (let i = 0; i < lines.length; i++) {
    // Determine if there is a comment in this line
    let commentIndex = _findVBScriptCommentIndex(lines[i]);

    // Replace all occurrences of the search string with the replace string unless it is inside a comment.
    lines[i] = lines[i].replace(
      searchString instanceof RegExp
        ? searchString
        : new RegExp(searchString, "g"),
      function (match) {
        if (commentIndex === false || lines[i].indexOf(match) < commentIndex) {
          return replaceString;
        } else {
          return match;
        }
      }
    );
  }

  // Re-join our lines together and return the modified code.
  return lines.join("\n");
}

/**
 * Find the index at which a comment starts on a line of vbscript code.
 *
 * @param {string} vbscriptLine A single line of vbscript code
 * @returns {number|false} Either the index at which the comment starts, or false if there are no comments on that line
 */
function _findVBScriptCommentIndex(vbscriptLine) {
  let commentIndex = -1; // initialize to -1, indicating that no comment has been found yet
  let insideString = false; // flag to track whether the current character is inside a string

  for (let i = 0; i < vbscriptLine.length; i++) {
    let char = vbscriptLine.charAt(i);
    if (char === '"' && !insideString) {
      // toggle insideString flag when a double quote is encountered
      insideString = true;
    } else if (char === '"' && insideString) {
      // toggle insideString flag again when another double quote is encountered
      insideString = false;
    } else if (char === "'" && !insideString) {
      // if a single quote is encountered and we're not inside a string, this is a comment; set commentIndex to i
      commentIndex = i;
      break; // break the loop because we've found a comment
    }
  }

  if (commentIndex !== -1) {
    return commentIndex;
  } else {
    return false;
  }
}

/**
 * Capitalize and Camel-case common vbscript and VPX global keywords / methods.
 *
 * @param {string} inputString The vbscript code to format
 * @returns {string} The formatted vbscript code
 */
function capitalizeKeywords(inputString) {
  /**
   * Keywords to search/replace.
   * The search does not use case sensitivity. Finds will be replaced according to the specified case in the array.
   * E.g. "Sub" will match any sub, case insensitive, and replace with "Sub".
   */
  const keywords = [
    // VBScript (taken from Geshi with a few modifications)
    "Empty",
    "Nothing",
    "Null",
    "vbArray",
    "vbBoolean",
    "vbByte",
    "vbCr",
    "vbCrLf",
    "vbCurrency",
    "vbDate",
    "vbDouble",
    "vbEmpty",
    "vbError",
    "vbFirstFourDays",
    "vbFirstFullWeek",
    "vbFirstJan1",
    "vbFormFeed",
    "vbFriday",
    "vbInteger",
    "vbLf",
    "vbLong",
    "vbMonday",
    "vbNewLine",
    "vbNull",
    "vbNullChar",
    "vbNullString",
    "vbObject",
    "vbSaturday",
    "vbSingle",
    "vbString",
    "vbSunday",
    "vbTab",
    "vbThursday",
    "vbTuesday",
    "vbUseSystem",
    "vbUseSystemDayOfWeek",
    "vbVariant",
    "vbWednesday",
    "False",
    "True",
    "bs",
    "Array",
    "Asc",
    "Atn",
    "CBool",
    "CByte",
    "CDate",
    "CDbl",
    "Chr",
    "CInt",
    "CLng",
    "Cos",
    "CreateObject",
    "CSng",
    "CStr",
    "Date",
    "DateAdd",
    "DateDiff",
    "DatePart",
    "DateSerial",
    "DateValue",
    "Day",
    "Eval",
    "Exp",
    "Filter",
    "Fix",
    "FormatDateTime",
    "FormatNumber",
    "FormatPercent",
    "GetObject",
    "Hex",
    "Hour",
    "InputBox",
    "InStr",
    "InstrRev",
    "Int",
    "IsArray",
    "IsDate",
    "IsEmpty",
    "IsNull",
    "IsNumeric",
    "IsObject",
    "Join",
    "LBound",
    "LCase",
    "Left",
    "Len",
    "Log",
    "LTrim",
    "Mid",
    "Minute",
    "Month",
    "MonthName",
    "MsgBox",
    "Now",
    "Oct",
    "Replace",
    "RGB",
    "Right",
    "Rnd",
    "Round",
    "RTrim",
    "ScriptEngine",
    "ScriptEngineBuildVersion",
    "ScriptEngineMajorVersion",
    "ScriptEngineMinorVersion",
    "Second",
    "Sgn",
    "Sin",
    "Space",
    "Split",
    "Sqr",
    "StrComp",
    "String",
    "StrReverse",
    "Tan",
    "Time",
    "TimeSerial",
    "TimeValue",
    "Trim",
    "TypeName",
    "UBound",
    "UCase",
    "VarType",
    "Weekday",
    "WeekdayName",
    "Year",
    "Call",
    "Case",
    "Const",
    "Dim",
    "Do",
    "Each",
    "Else",
    "End",
    "Erase",
    "Execute",
    "Exit",
    "For",
    "Function",
    "GoSub",
    "GoTo",
    "If",
    "Loop",
    "Next",
    "On Error",
    "Option Explicit",
    "Private",
    "Public",
    "Randomize",
    "ReDim",
    "Rem",
    "Resume",
    "Select",
    "Set",
    "Sub",
    "Then",
    "Wend",
    "While",
    "With",
    "In",
    "To",
    "Step",
    "And",
    "Eqv",
    "Imp",
    "Is",
    "Mod",
    "Not",
    "Or",
    "Xor",

    // VPX Globals
    "GameTime",
    "SystemTime",
    "GetCustomParam",
    "NightDay",
    "LeftFlipperKey",
    "RightFlipperKey",
    "LeftTiltKey",
    "RightTiltKey",
    "CenterTiltKey",
    "PlungerKey",
    "StartGameKey",
    "AddCreditKey",
    "AddCreditKey2",
    "LeftMagnaSave",
    "RightMagnaSave",
    "LockbarKey",
    "ActiveBall",
    "ActiveTable",
    "ShowDT",
    "ShowFSS",
    "WindowWidth",
    "WindowHeight",
    "DMDWidth",
    "DMDHeight",
    "DMDPixels",
    "DMDColoredPixels",
    "RenderingMode",
    "Nudge",
    "NudgeGetCalibration",
    "NudgeSetCalibration",
    "NudgeSensorStatus",
    "NudgeTiltStatus",
    "PlaySound",
    "StopSound",
    "PlayMusic",
    "MusicVolume",
    "EndMusic",
    "FireKnocker",
    "QuitPlayer",
    "Version",
    "VPBuildVersion",
    "VersionMajor",
    "VersionMinor",
    "VersionRevision",
    "GetBalls",
    "GetElements",
    "GetElementByName",
    "UpdateMaterial",
    "GetMaterial",
    "UpdateMaterialPhysics",
    "GetMaterialPhysics",
    "MaterialColor",
    "GetSerialDevices",
    "OpenSerial",
    "CloseSerial",
    "FlushSerial",
    "SetupSerial",
    "ReadSerial",
    "WriteSerial",
    "LoadValue",
    "SaveValue",
    "UserDirectory",
  ];

  // Loop through every keyword and search for the case insensitive version in the vbscript code, replacing it with the case sensitive version.
  for (let i = 0; i < keywords.length; i++) {
    let regex = new RegExp("\\b(" + keywords[i] + ")\\b", "gi");
    inputString = _replaceVBScriptCode(inputString, regex, keywords[i]);
  }

  return inputString;
}

/**
 * When code is chained together on the same line with a colon, break it up onto new lines and remove the colons.
 *
 * @param {string} vbscriptCode The vbscript code
 * @returns {string} The modified vbscript code
 */
function removeChainedCode(vbscriptCode) {
  let stack = []; // List of end statements we must ensure exists

  // Split and process code line by line
  let lines = vbscriptCode.split("\n");
  for (let i = 0; i < lines.length; i++) {
    // Trim the line of whitespace, and calculate length
    const trimmedLine = lines[i].trim();
    let line = trimmedLine;
    let endChar = line.length;

    // If a comment exists, determine the line length to end at the start of the comment (so as to ignore comments)
    let commentIndex = _findVBScriptCommentIndex(line);
    if (commentIndex !== false) {
      endChar = commentIndex;
    }

    const trimmedLineNoComments =
      commentIndex !== false ? line.substring(0, commentIndex) : line;

    // Split line into segments delimited by quotes
    let segments = [];
    let currentSegment = "";
    let inQuotes = false;
    let escapeNext = false;
    for (let j = 0; j < endChar; j++) {
      let char = line.charAt(j);

      if (escapeNext) {
        currentSegment += char;
        escapeNext = false;
      } else if (char === "\\") {
        escapeNext = true;
        currentSegment += char;
      } else if (char === '"') {
        inQuotes = !inQuotes;
        currentSegment += char;
      } else if (char === ":" && !inQuotes) {
        // We found a colon outside of a quotation that must be split into multiple lines.
        // If the colon is at the end of the line, then there is no code to separate.
        if (j < endChar - 1) {
          segments.push(currentSegment.trim());
        }
        currentSegment = "";
      } else {
        currentSegment += char;
      }
    }

    // No colons were found on this line that we need to split, so there is nothing to edit. Go to the next line.
    if (segments.length < 1) {
      continue;
    }

    segments.push(currentSegment.trim());

    // Special case: If any "End If" statements existed, then split all "If/Then" onto a new line to prevent indentation breakages.
    let hasEndIf = false;
    segments = segments
      .reverse()
      .map((segment) => {
        if (segment.toLowerCase().startsWith("end if")) {
          hasEndIf = true;
        }

        if (!hasEndIf) {
          return segment;
        }

        segment.toLowerCase().replace(")then ", ") Then ");
        let thenIndex = segment.toLowerCase().indexOf(" then ");
        if (thenIndex < 0) {
          return segment;
        }

        return (
          segment.substring(0, thenIndex + 5) +
          "\n" +
          segment.substring(thenIndex + 6)
        );
      })
      .reverse();

    // Join each segment onto a new line (effectively putting the code separated by colons onto new lines)
    line = segments.join("\n");

    // Determine what end statements must exist
    if (line.toLowerCase().startsWith("if ")) {
      stack.push("End If");
    } else if (line.toLowerCase().startsWith("select")) {
      stack.push("End Select");
    } else if (line.toLowerCase().startsWith("for ")) {
      stack.push("Next");
    } else if (line.toLowerCase().startsWith("do ")) {
      stack.push("Loop");
    } else if (
      line.toLowerCase().startsWith("function ") ||
      line.toLowerCase().startsWith("public function ") ||
      line.toLowerCase().startsWith("private function ")
    ) {
      stack.push("End Function");
    } else if (
      line.toLowerCase().startsWith("sub ") ||
      line.toLowerCase().startsWith("public sub ") ||
      line.toLowerCase().startsWith("private sub ")
    ) {
      stack.push("End Sub");
    } else if (
      line.toLowerCase().startsWith("property ") ||
      line.toLowerCase().startsWith("public property ") ||
      line.toLowerCase().startsWith("private property ")
    ) {
      stack.push("End Property");
    } else if (line.toLowerCase().startsWith("class ")) {
      stack.push("End Class");
    } else if (line.toLowerCase().startsWith("while ")) {
      stack.push("Wend");
    } else if (line.toLowerCase().startsWith("with ")) {
      stack.push("End With");
    }

    // If a comment existed on the original line, start with the comment first, new line, and then the code.
    if (commentIndex !== false) {
      lines[i] = trimmedLine.substring(commentIndex);
      if (line.length > 0) {
        lines[i] += "\n" + line;
      }
    } else {
      lines[i] = line;
    }

    // Determine which of the end statements already exists; we do not need to add them again or we will break the code.
    if (stack.length > 0) {
      lines[i]
        .split("\n")
        .reverse()
        .forEach((nLine) => {
          if (stack.length > 0) {
            if (
              stack[stack.length - 1].toLowerCase() ===
              nLine.trim().toLowerCase()
            ) {
              stack.pop();
            }
          }
        });
    }

    // For each of the end statements missing, we must add them on a new line.
    while (stack.length > 0) {
      lines[i] += "\n" + stack.pop();
    }
  }

  // Join lines into a single string, returning the modified vbscript code.
  return lines.join("\n");
}

/**
 * Re-formats the indentation of the vbscript code using tabs based on the code blocks.
 * Note: Any lines chaining code together with colons may break this. Syntax errors may also break this.
 *
 * @param {string} vbscriptCode The vbscript code
 * @returns {string} The re-indented code
 */
function fixIndentation(vbscriptCode) {
  // Split the code into lines
  const lines = vbscriptCode.split("\n");

  // Initialize variables for tracking indentation level
  let currentIndentation = 0;
  let indentation = "";

  // Loop through each line of code
  for (let i = 0; i < lines.length; i++) {
    let line = lines[i].trim();

    // Special fix: If there is a parenthesis after if without a space, add one to ensure indentations work
    if (line.toLowerCase().startsWith("if(")) {
      line = "If " + line.substring(2);
    }

    // Special fix: If there is a parenthesis before "then" without a space, add one to ensure indentations work
    if (line.toLowerCase().includes(")then")) {
      let parenthIndex = line.toLowerCase().indexOf(")then");
      line =
        line.substring(0, parenthIndex) +
        ") Then" +
        line.substring(parenthIndex + 5);
    }

    const commentIndex = _findVBScriptCommentIndex(line);
    const lineWithoutComment = line
      .substring(0, commentIndex !== false ? commentIndex : undefined)
      .trim();

    // The keywords below will match the start of the line if they end in a space, else the line must match exactly. Should be written in lower case.
    // If a keyword is matched both in starters and enders, then the line itself will have one less indentation, but proceeding lines will have one more indentation.

    // Keywords that indicate the proceeding lines should have an indentation level one more.
    const starters = [
      "case ",
      "do",
      "do ",
      "else",
      "elseif ",
      "for ",
      "function ",
      "public function ",
      "private function ",
      "if ",
      "select case ",
      "sub ",
      "public sub ",
      "private sub ",
      "property ",
      "public property ",
      "private property ",
      "while ",
      "with ",
      "class ",
    ];

    // Keywords that indicate this and proceeding lines should have an indentation level one less.
    const enders = ["else", "elseif ", "end ", "loop", "next", "wend", "case "];

    // If the line ends a code block, decrease the indentation level for this and proceeding lines.
    for (let i2 = 0; i2 < enders.length; i2++) {
      if (
        (enders[i2].endsWith(" ") &&
          line.toLowerCase().startsWith(enders[i2])) ||
        lineWithoutComment.toLowerCase() === enders[i2] // Lines may have comments, which we want to ignore
      ) {
        currentIndentation--;

        // Special case: We need to decrease indent two levels for "end select" due to how the indentations for "case" works.
        if (lineWithoutComment.toLowerCase() === "end select") {
          currentIndentation--;
        }

        if (currentIndentation < 0) {
          console.error("Negative indentation level on line " + i);
          return (
            lines.filter((value, index) => index < i).join("\n") +
            "\n" +
            "ERROR: Negative indentation level"
          );
        }

        indentation = "\t".repeat(currentIndentation);
      }
    }

    // Add the current indentation to the line
    lines[i] = indentation + line;

    // If the line starts a new code block, increase the indentation level for proceeding lines
    for (let i2 = 0; i2 < starters.length; i2++) {
      if (
        (starters[i2].endsWith(" ") &&
          line.toLowerCase().startsWith(starters[i2])) ||
        lineWithoutComment.toLowerCase() === starters[i2] // Lines may have comments, which we want to ignore
      ) {
        // Special case: If there's anything after a "Then" on the same line (excluding a comment), assume this if condition will not have an end if.
        if (
          starters[i2] === "if " &&
          lineWithoutComment.toLowerCase().includes(" then ")
        ) {
          continue;
        }
        // Special case: We need to indent two levels for "select case" due to how the indentations for "case" works.
        if (starters[i2] === "select case ") {
          indentation += "\t";
          currentIndentation++;
        }

        indentation += "\t";
        currentIndentation++;
      }
    }
  }

  // Join the lines back together and return the result
  return lines.join("\n");
}
